module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
        mocha: true,
    },
    extends: ['eslint:recommended', 'react-app', 'react-app/jest'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
        cy: 'readonly',
        expect: 'readonly',
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2020,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        eqeqeq: ['error', 'always'],
    },
};
